interface ApiResponse {
  base: string;
  disclaimer: string;
  license: string;
  rates: Rates;
  timestamp: number;
}

type Rate = {
    key: string;
    value: number
}

type Rates = {
  [key: string]: number;
};
