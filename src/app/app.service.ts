import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

const APP_ID = 'cb7dfcfb093f48739207d550ff2a22b0';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable()
export class AppService {
  constructor(private http: HttpClient) {}

  getCurrencies() {
    return this.http.get('https://openexchangerates.org/api/latest.json?app_id=' + APP_ID);
  }

  /**
   * allows user only to type in real cases
   * e.g. 0.10 | .10 |  10.10 | 10
   */
  formatValue(val: string) {
    let value = val.match(/\d+\.+\d+|\.+\d+|\d+\.|\d+|\./);
    return (value && value[0]) || '';
  }

  calculateToUsd(amount: string, rate: number) {
    if (!amount) {
      return 0;
    }

    let value = parseFloat(amount) / rate;
    return isNaN(value) ? 0 : value;
  }

  calculateFromUsd(usd: string, rate: number) {
    if (!usd) {
      return 0;
    }

    let value = parseFloat(usd) * rate;
    return isNaN(value) ? 0 : value;
  }
}
