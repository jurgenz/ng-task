import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { AppService } from './app.service';
import { FormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { ShuffleComponent } from './shuffle/shuffle.component';
import { RatesComponent } from './rates/rates.component';

@NgModule({
  declarations: [AppComponent, ShuffleComponent, RatesComponent],
  imports: [BrowserModule, HttpClientModule, FormsModule],
  providers: [AppService],
  bootstrap: [AppComponent]
})
export class AppModule {}
