import { Component, Input } from '@angular/core';
import { AppService } from './app.service';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  @Input('NgModel') FromInput: string | number = 0;
  @Input('NgModel') ToInput: string | number = '';

  title: string = 'Currency converter';
  from: string = 'USD';
  to: string = 'EUR';
  rate: number = 0;
  timestamp: null | number = null;
  rates: Rates = {};

  constructor(private _appService: AppService) {}

  ngOnInit() {
    this._appService.getCurrencies().subscribe((data: ApiResponse) => {
      this.timestamp = data.timestamp;
      this.rates = data.rates;
      this.rate = data.rates.EUR;
    });
  }

  getFormattedDate() {
    if (!this.timestamp) {
      return '';
    }
    return 'Last updated: ' + new Date(this.timestamp * 1000).toLocaleDateString();
  }

  onFromChange(e: Event) {
    let input = e.target as HTMLInputElement;
    const value = this._appService.formatValue(input.value);

    this.FromInput = value;
    this.ToInput = this._appService.calculateFromUsd(value, this.rate);
  }

  onToChange(e: Event) {
    let input = e.target as HTMLInputElement;
    const value = this._appService.formatValue(input.value);

    this.ToInput = value;
    this.FromInput = this._appService.calculateToUsd(value, this.rate);
  }

  setActiveCurrency(rate: Rate) {
    this.to = rate.key;
    this.rate = rate.value;
    this.ToInput = this._appService.calculateFromUsd(this.FromInput.toString(), this.rate);
  }
}
