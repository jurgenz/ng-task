import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';

@Component({
  selector: 'app-rates',
  templateUrl: './rates.component.html',
  styleUrls: ['./rates.component.scss']
})
export class RatesComponent implements OnInit {
  @Input() rates: Rates;
  @Input() to: string;

  @Output() onSelect: EventEmitter<any> = new EventEmitter();

  onCurrencySelect(currency) {
    this.onSelect.emit(currency);
  }

  constructor() {}

  ngOnInit() {}
}
